var Models = (function () {
    var D = DataService.getInstance(),
        currentYear = new Date().getFullYear(),
        collectionNormalize = function (collection) {
            _.each(collection, function (item) {
                var extension = (item['FileRef'].split('.').pop()).toLowerCase();

                if (extension === 'jpg' || extension === 'png' || extension === 'bmp') {
                    item['imageUrl'] = '/' + helper.removeID(item['FileRef']);
                } else {
                    item['imageUrl'] = '';
                }
                item['Year'] = helper.removeID(item['Year']);
                item['DateStart'] = helper.changeDate(item['DateStart']);
                item['Employees'] = item['Employees'] ? parseFloat(item['Employees']) : '';
                item['Hours'] = item['Hours'] ? parseFloat(item['Hours']) : '';
                item['SupportedPeople'] = item['SupportedPeople'] ? parseFloat(item['SupportedPeople']) : '';
                item['FundsRaised'] = item['FundsRaised'] ? parseFloat(item['FundsRaised']) : '';
                item['DescriptionShort'] = item['Description0'] ? item['Description0'].trunc(60) : '';
            });
        };

    return {
        StatBox: Backbone.Model.extend({
            sync: function () {
                var self = this,
                    queryOpt = {};

                queryOpt[this.listTitle] = {
                    webURL: '/sites/activityreporting/',
                    CAMLQuery: '<Query><Where><Eq><FieldRef Name="Year" /><Value Type="Text">' + currentYear + '</Value></Eq></Where></Query>'
                };

                D.get([self.listTitle], queryOpt).then(function (resObj) {
                    var resArray = resObj[self.listTitle]['array'],
                        sum = {
                            activities: 0,
                            employees: 0,
                            people: 0,
                            hours: 0
                        };

                    sum['activities'] = resArray.length;
                    _.each(resArray, function (item) {
                        sum['employees'] = sum['employees'] + parseFloat(item['Employees'] || 0);
                        sum['people'] = sum['people'] + parseFloat(item['SupportedPeople'] || 0);
                        sum['hours'] = sum['hours'] + parseFloat(item['Hours'] || 0);
                    });
                    sum = _.mapObject(sum, function (val) {
                        return helper.numberWithCommas(val);
                    });
                    self.attributes = sum;
                    self.trigger('sync:done');
                });
            },
            initialize: function (opt) {
                this.listTitle = opt.listTitle;
            }
        }),

        SurveyResults: Backbone.Model.extend({
            filter: function (filterParam) {
                var filtered = [].concat(this.initCollection);

                if (filterParam.year) {
                    filtered = _.filter(filtered, function (item) {
                        return item['Year'] === filterParam.year;
                    })
                }
                if (filterParam.country) {
                    filtered = _.filter(filtered, function (item) {
                        return item['Country'] === filterParam.country;
                    })
                }
                if (filterParam.type) {
                    filtered = _.filter(filtered, function (item) {
                        return item['ActivityType'] === filterParam.type;
                    })
                }
                if (filterParam.employeesMin) {
                    filtered = _.filter(filtered, function (item) {
                        return item['Employees'] >= filterParam.employeesMin;
                    })
                }
                if (filterParam.employeesMax) {
                    filtered = _.filter(filtered, function (item) {
                        return item['Employees'] <= filterParam.employeesMax;
                    })
                }
                this.attributes.collection = filtered;
            },
            initialize: function (initList) {
                var self = this,
                    queryOpt = {};

                this.initList = initList;
                queryOpt[this.initList] = {
                    webURL: '/sites/activityreporting/',
                    CAMLQuery: '<Query><OrderBy><FieldRef Name="Created" Ascending="FALSE"/></OrderBy><Where></Where></Query>'
                };

                D.get([initList], queryOpt).then(function (resObj) {
                    var resArray = resObj[initList]['array'],
                        countries = [],
                        types = [],
                        years = [];

                    resArray = _.filter(resArray, function(item) {
                        return item['Title'];
                    });
                    collectionNormalize(resArray);

                    _.each(resArray, function (item) {
                        years.push(item['Year']);
                        countries.push(item['Country']);
                        types.push(item['ActivityType']);
                    });

                    years = (_.uniq(years)).sort();
                    countries = (_.uniq(countries)).sort();
                    types = (_.uniq(types)).sort();

                    self.initCollection = [].concat(resArray);
                    self.attributes = {
                        collection: resArray,
                        years: years,
                        countries: countries,
                        types: types
                    };
                    self.trigger('init:done');
                });
            }
        }),

        RecentActivities: Backbone.Model.extend({
            initialize: function (opt) {
                var self = this,
                    queryOpt = {};

                queryOpt[opt.initList] = {
                    webURL: '/sites/activityreporting/',
                    CAMLRowLimit: opt.limit,
                    CAMLQuery: '<Query><OrderBy><FieldRef Name="Created" Ascending="FALSE"/></OrderBy><Where><Eq><FieldRef Name="Year" /><Value Type="Text">' + currentYear + '</Value></Eq></Where></Query>'
                };

                D.get([opt.initList], queryOpt).then(function (resObj) {
                    var resArray = resObj[opt.initList]['array'];

                    resArray = _.filter(resArray, function(item) {
                        return item['Title'];
                    });
                    collectionNormalize(resArray);
                    self.attributes = resArray;
                    self.trigger('init:done');
                });
            }
        })
    }
})();