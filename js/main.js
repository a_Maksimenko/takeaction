(function ($) {
    helper.viewRenderer({
        '.statistic-box': Views.StatBox,
        '.survey-results-holder': Views.SurveyResults,
        '.recent-activities-holder': Views.RecentActivities
    });

    $('.mainContent').on('click', '.report-btn', function (e) {
        e.preventDefault();
        $.colorbox({
            iframe: true,
            innerWidth: 540,
            innerHeight: 550,
            href: '/sites/activityreporting/System/html/survey.html',
            closeButton: false
        });
    });
})(jQuery);