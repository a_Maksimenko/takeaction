var helper = (function ($) {
    String.prototype.trunc = function (n) {
        return this.substr(0, n - 1) + (this.length > n ? '...' : '');
    };

    return {
        parseDate: function (input) {
            var parts = input.split(' ')[0].split('-');
            return new Date(parts[0], parts[1] - 1, parts[2]);
        },
        changeDate: function (date, getMonth) {
            var newDate = helper.parseDate(date),
                month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"][newDate.getMonth()];

            if (!getMonth) {
                return newDate.getDate() + ' ' + month + ' ' + newDate.getFullYear();
            } else {
                return month;
            }
        },
        removeID: function (string) {
            if (string) {
                var result = string.split(';#')[1];

                if (result) {
                    return result;
                } else {
                    return string;
                }
            } else {
                return '';
            }
        }
    };
})(jQuery);