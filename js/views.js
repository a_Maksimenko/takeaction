var Views = (function ($) {
    return {
        StatBox: Backbone.View.extend({
            Model: Models.StatBox,
            template: _.template($('#template-stat-box').html()),
            initialize: function () {
                var listTitle = this.$el.attr('data-list'),
                    model = new this.Model({listTitle: listTitle});

                model.sync();
                model.on('sync:done', function () {
                    this.render(model.attributes);
                }, this);
            },
            render: function (item) {
                var compiledHTML = this.template({item: item});

                this.$el
                    .html(compiledHTML)
                    .show();
            }
        }),

        SurveyResults: Backbone.View.extend({
            filterParam: {},
            Model: Models.SurveyResults,
            template: _.template($('#template-survey-results').html()),
            subTemplate: _.template($('#template-survey-results-list').html()),
            popupTemplate: _.template($('#template-survey-results-popup').html()),
            initialize: function () {
                var initList = this.$el.attr('data-list');

                this.limit = this.step = this.$el.attr('data-limit');
                this.model = new this.Model(initList);
                this.model.on('init:done', function () {
                    this.render();
                    this.$subEl = this.$el.find('.filteredList');
                    this.subRender();
                }, this);
            },
            render: function () {
                var compiledHTML = this.template({
                    countries: this.model.attributes.countries,
                    types: this.model.attributes.types,
                    years: this.model.attributes.years
                });

                this.$el.html(compiledHTML);
            },
            subRender: function () {
                var limit = this.$el.attr('data-limit'),
                    compiledHTML = this.subTemplate({
                        collection: this.model.attributes.collection,
                        limit: this.limit
                    });

                this.$subEl.html(compiledHTML);
                this.addEvenClass();
            },
            addEvenClass: function () {
                this.$subEl.find('tbody tr:even').addClass('even');
            },
            events: {
                'change .filter-list': 'changeFilter',
                'click .filter-apply': 'applyFilter',
                'click .show-more': 'showMore',
                'click .survey-item': 'showDetails'
            },
            showMore: function () {
                this.limit = this.limit + this.step;
                this.subRender();
            },
            changeFilter: function (e) {
                var $target = $(e.currentTarget),
                    key = $target.attr('name'),
                    val = $target.val(),
                    text = $target.find('option:selected').text();

                if (key === 'employees') {
                    switch (val) {
                        case '':
                            this.filterParam['employeesMin'] = false;
                            this.filterParam['employeesMax'] = false;
                            break;
                        case '1-5':
                            this.filterParam['employeesMin'] = 1;
                            this.filterParam['employeesMax'] = 5;
                            break;
                        case '>5-10':
                            this.filterParam['employeesMin'] = 6;
                            this.filterParam['employeesMax'] = 10;
                            break;
                        case '>10-20':
                            this.filterParam['employeesMin'] = 11;
                            this.filterParam['employeesMax'] = 20;
                            break;
                        case '>20':
                            this.filterParam['employeesMin'] = 21;
                            this.filterParam['employeesMax'] = false;
                            break;
                    }
                } else {
                    this.filterParam[key] = val;
                }
                $target.prev().text(text);
            },
            applyFilter: function () {
                this.model.filter(this.filterParam);
                this.subRender();
            },
            showDetails: function (e) {
                var $target = $(e.currentTarget),
                    itemID = $target.attr('data-id'),
                    item = _.findWhere(this.model.attributes.collection, {ID: itemID}),
                    compiledHTML = this.popupTemplate({item: item});

                $.colorbox({
                    html: compiledHTML,
                    innerWidth: 540,
                    closeButton: false
                });
                e.preventDefault();
            }
        }),

        RecentActivities: Backbone.View.extend({
            filterParam: {},
            Model: Models.RecentActivities,
            template: _.template($('#template-recent-activities').html()),
            popupTemplate: _.template($('#template-survey-results-popup').html()),
            initialize: function () {
                this.model = new this.Model({
                    initList: this.$el.attr('data-list'),
                    limit: this.$el.attr('data-limit')
                });
                this.model.on('init:done', function () {
                    this.render();
                }, this);
            },
            render: function () {
                var compiledHTML = this.template({collection: this.model.attributes});

                this.$el
                    .html(compiledHTML)
                    .show();
            },
            events: {
                'click .show-more': 'showDetails'
            },
            showDetails: function (e) {
                var $target = $(e.currentTarget),
                    itemID = $target.attr('data-id'),
                    item = _.findWhere(this.model.attributes, {ID: itemID}),
                    compiledHTML = this.popupTemplate({item: item});

                $.colorbox({
                    html: compiledHTML,
                    innerWidth: 540,
                    closeButton: false
                });
                e.preventDefault();
            }
        })
    }
})(jQuery);